#!/bin/bash -x

archive_name=$1

touch dummy
tar -cvf /tmp/${archive_name}.tar dummy
# tar --delete -f ${archive_name}.tar dummy

cd /
for configfiles in `cat /tmp/backupfiles | xargs -n1 `
do
    for configfile in ${configfiles}
    do
        test -f "${configfile}" -a "${configfile}" != ''
        if [ $? -eq 0 ];  then
            tar -rvf /tmp/${archive_name}.tar .${configfile} 2>/dev/null
        else
            echo "Bad Backupfiles Expression"
            exit -1
        fi
    done
done
cd /tmp
gzip ${archive_name}.tar
