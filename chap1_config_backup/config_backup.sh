#!/bin/bash

# License: Apache License
# usage
_usage="
    === usage  ===
    本スクリプトは、コンフィグファイルをバックアップします。

    === How To ===
    configファイルを各種サーバから、手元のbackup/hostnameフォルダにバックアップします。
"

# ----- user variable definition -----

select_inventory="./inventories/development"
playbook="./site.yml"

flg_directexc=0
flg_step=1

ansible_options="--connection=ssh  -K "

# ----- variable definition -----


_loop=0
dir_base=`pwd`
LOGFILE=`date "+%Y%m%d_%H%M%S"`

inventories=()
inventories_index=1

operations=(
    select_inventories
)
export ANSIBLE_SSH_ARGS="-F ansible.sshconfig -vvvv"


# ----- function _definition -----
function usage_exit(){
    echo $_usage
    exit 1
}



function _init(){
    _ansible_bin=`which ansible-playbook`
    _check_files
    _fetch_inventories
}

function _check_files(){
    if [ ! -e $_ansible_bin ]; then
        echo "ERROR: Ansible Binary is not Exist."
        exit -1
    fi
}

function _fetch_inventories(){
    for inventory in ./inventories/*
    do
        if [ ${inventory} == ${select_inventory} ]; then
            echo ${#inventories[@]}
            inventories_index=${#inventories[@]}
        fi
        inventories=(${inventories[@]} "${inventory}")
    done
}


function _list_menu(){
    printf "Base Directory: %s\n"  dir
    printf "Current Config:\n"
    printf "  Current Inventory\n" inventory
    printf " \n"

}
function _list_inventories(){
    printf "==== Inventories ====\n"
    index=0
    for inventory in ${inventories[@]}
    do
        printf "[%2d] : %s\n" $index $inventory
        index=`expr $index + 1`
    done

    printf "\n"
}

function _select_index(){
    printf "select index:"

    read inventories_index
    while [ $inventories_index == '' -o -f '${inventories[$inventories_index]}'  ]
    do
        printf "select index:"
        read inventories_index
    done
}


function _exc_playbook(){
    if [ ! ${flg_step} -eq 0 ]; then
        ansible-playbook -i ${inventories[$inventories_index]} --extra-vars "LOGFILE=${LOGFILE}" $playbook ${ansible_options}
    elif [ ${flg_step} -eq 1 ]; then
        ansible-playbook -i ${inventories[$inventories_index]} --extra-vars "LOGFILE=${LOGFILE}" $playbook --step ${ansible_options}
    fi
}

function _main(){
    _init
    while [ ${_loop} -eq 0 ]
    do
        # ToDo
        if [  ${flg_directexc} -eq  1 ] ; then
            _loop=1
            _exc_playbook
	        exit 0
	    else
	        _loop=1
	        inventories_index=
	        _list_inventories
	        _select_index
            _exc_playbook
	        exit 0
        fi
    done
}


_main
